import * as mysql from "mysql2/promise";
import { env } from "../utils/types";

const PORT = parseInt(env("DB_PORT"));
const HOST = env("DB_HOST");
const USER = env("DB_USER_NAME");
const PASSWORD = env("DB_USER_PASSWORD");
const DB = env("DB_NAME");

export async function connect() {
  const conn = await mysql.createPool({
    host: HOST,
    user: USER,
    password: PASSWORD,
    database: DB,
    port: PORT,
  });
  return conn;
}
