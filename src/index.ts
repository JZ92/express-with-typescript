import express, { Request, Response, NextFunction } from "express";
import morgan from "morgan";
import cors from "cors";
import { env } from "./utils/types";

import user_router from "./modules/user.router";

import {
  error_handler,
  error_handler2,
  not_found,
} from "./middlewares/error.handler";

const PORT = parseInt(env("PORT"));
const HOST = env("HOST");

const app = express();

const logger = (req: Request, res: Response, next: NextFunction) => {
  console.log(`${req.method} ${req.path}`);
  next();
};

app.use(logger);

// Middlewares
app.use(cors());
app.use(morgan("dev"));

// Routing

app.use("/api/users", user_router);

// Central error handling
app.use(error_handler);
app.use(error_handler2);

// When no routes were matched...
app.use("*", not_found);

// Start the express api server
(async () => {
  await app.listen(PORT, HOST);
  console.log(`api is live on`, ` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);
})().catch(console.log);
