import raw from "../middlewares/route.async.wrapper";
import express, { Request, Response, NextFunction } from "express";
import Joi from "joi";
import { connect } from "../db/mysql.connection";

const router = express.Router();

let connection: any;

(async () => {
  connection = await connect();
})();

const TABLE = "MOCK_DATA";

// Parse json req.body on post routes
router.use(express.json());

// GET ALL USERS
router.get(
  "/",
  raw(async (req: Request, res: Response) => {
    const getResponse = await connection.query(`SELECT * FROM ${TABLE};`);
    res.status(200).json(getResponse[0]);
  })
);

// CREATE NEW USER
router.post(
  "/",
  raw(async (req: Request, res: Response) => {
    console.log(req.body, "create a user, req.body:");

    const schema = Joi.object({
      first_name: Joi.string().min(3).max(30).required(),
      last_name: Joi.string().min(3).max(30).required(),
      email: Joi.string().email().required(),
    });

    const { error, value } = schema.validate(req.body);
    if (error) {
      throw new Error(
        `Validation error: ${error.details.map((x) => x.message).join(", ")}`
      );
    } else {
      req.body = value;
    }

    const queryResponse = await connection.query(
      `INSERT INTO ${TABLE} (first_name, last_name, email) VALUES("${req.body.first_name}", "${req.body.last_name}", "${req.body.email}");`
    );
    const getResponse = await connection.query(
      `SELECT * FROM ${TABLE} ORDER BY ID DESC LIMIT 1`
    );
    if (queryResponse[0].affectedRows > 0) res.status(200).json(getResponse[0]);
  })
);

// GET WTIH PAGINATION
router.get(
  "/paginate/:page?/:items?",
  raw(async (req: Request, res: Response) => {
    console.log(req.params, "get all users, req.params:");
    let { page = 0, items = 10 } = req.params;
    const getResponse = await connection.query(
      `SELECT * FROM ${TABLE} LIMIT ${items} OFFSET ${page};`
    );

    res.status(200).json(getResponse[0]);
  })
);

// GET A SINGLE USER
router.get(
  "/:id",
  raw(async (req: Request, res: Response) => {
    const getResponse = await connection.query(
      `SELECT * FROM ${TABLE} WHERE id=${req.params.id};`
    );
    res.status(200).json(getResponse[0][0]);
  })
);

// UPDATE A WHOLE USER
router.put(
  "/:id",
  raw(async (req: Request, res: Response) => {
    const schema = Joi.object({
      first_name: Joi.string().min(3).max(30).required(),
      last_name: Joi.string().min(3).max(30).required(),
      email: Joi.string().email().required(),
    });

    const { error, value } = schema.validate(req.body);
    if (error) {
      throw new Error(
        `Validation error: ${error.details.map((x) => x.message).join(", ")}`
      );
    } else {
      req.body = value;
    }
    const getResponse = await connection.query(
      `SELECT * FROM ${TABLE} WHERE id=${req.params.id};`
    );
    const user = getResponse[0][0];
    const queryResponse = await connection.query(`UPDATE ${TABLE}
    SET first_name = "${req.body.first_name}", last_name = "${req.body.last_name}", email = "${req.body.email}"
    WHERE id = ${req.params.id};`);
    if (queryResponse[0].affectedRows > 0) res.status(200).json(user);
    else
      res
        .status(404)
        .json({ status: `User number ${req.params.id} is not found` });
  })
);

// UPDATE SOME PROPERTIES IN USER
router.patch(
  "/:id",
  raw(async (req: Request, res: Response) => {
    const schema = Joi.object({
      first_name: Joi.string().min(3).max(30),
      last_name: Joi.string().min(3).max(30),
      email: Joi.string().email(),
    });

    const { error, value } = schema.validate(req.body);
    if (error) {
      throw new Error(
        `Validation error: ${error.details.map((x) => x.message).join(", ")}`
      );
    } else {
      req.body = value;
    }

    const getResponse = await connection.query(
      `SELECT * FROM ${TABLE} WHERE id=${req.params.id};`
    );
    const user = getResponse[0][0];

    let patchQuery = `UPDATE ${TABLE} SET `;
    Object.keys(req.body).map(
      (property) => (patchQuery += `${property} = "${req.body[property]}", `)
    );
    patchQuery = patchQuery.slice(0, patchQuery.length - 2);
    patchQuery += ` WHERE id = ${req.params.id};`;

    const queryResponse = await connection.query(patchQuery);

    if (queryResponse[0].affectedRows > 0) res.status(200).json(user);
    else
      res
        .status(404)
        .json({ status: `User number ${req.params.id} is not found` });
  })
);

// DELETE A USER
router.delete(
  "/:id",
  raw(async (req: Request, res: Response) => {
    const getResponse = await connection.query(
      `SELECT * FROM ${TABLE} WHERE id=${req.params.id};`
    );
    const user = getResponse[0][0];
    const queryResponse = await connection.query(`DELETE FROM ${TABLE}
    WHERE id = ${req.params.id};`);
    if (queryResponse[0].affectedRows > 0) res.status(200).json(user);
    else
      res
        .status(404)
        .json({ status: `User number ${req.params.id} is not found` });
  })
);

export default router;
